<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmpresaRepository")
 */
class Empresa
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomeEmpresa;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $enderecoEmpresa;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telefoneEmpresa;

    /**
     * @var ArrayCollection Coleção de socios
     * @ORM\ManyToMany(targetEntity="Socio", mappedBy="empresas")
     */
    private $socios;

    public function __construct() {
        $this->socios = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomeEmpresa(): ?string
    {
        return $this->nomeEmpresa;
    }

    public function setNomeEmpresa(string $nomeEmpresa): self
    {
        $this->nomeEmpresa = $nomeEmpresa;

        return $this;
    }

    public function getEnderecoEmpresa(): ?string
    {
        return $this->enderecoEmpresa;
    }

    public function setEnderecoEmpresa(string $enderecoEmpresa): self
    {
        $this->enderecoEmpresa = $enderecoEmpresa;

        return $this;
    }

    public function getTelefoneEmpresa(): ?string
    {
        return $this->telefoneEmpresa;
    }

    public function setTelefoneEmpresa(string $telefoneEmpresa): self
    {
        $this->telefoneEmpresa = $telefoneEmpresa;

        return $this;
    }

    public function getSocios()
    {
        return $this->socios;
    }

    public function setSocios(ArrayCollection $socios)
    {
        $this->socios = $socios;

        return $this;
    }

    
}
