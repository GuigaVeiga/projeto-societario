<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SocioRepository")
 */
class Socio
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomeSocio;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $enderecoSocio;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telefoneSocio;

     /**
     * @var ArrayCollection Coleção de empresas
     * @ORM\ManyToMany(targetEntity="Empresa", inversedBy="socios")
     * @ORM\JoinTable(name="socios_empresas")
     */
    private $empresas;

    public function __construct() {
        $this->empresas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomeSocio(): ?string
    {
        return $this->nomeSocio;
    }

    public function setNomeSocio(string $nomeSocio): self
    {
        $this->nomeSocio = $nomeSocio;

        return $this;
    }

    public function getEnderecoSocio(): ?string
    {
        return $this->enderecoSocio;
    }

    public function setEnderecoSocio(string $enderecoSocio): self
    {
        $this->enderecoSocio = $enderecoSocio;

        return $this;
    }

    public function getTelefoneSocio(): ?string
    {
        return $this->telefoneSocio;
    }

    public function setTelefoneSocio(string $telefoneSocio): self
    {
        $this->telefoneSocio = $telefoneSocio;

        return $this;
    }

    public function getEmpresas()
    {
        return $this->empresas;
    }

    public function setEmpresas(ArrayCollection $empresas)
    {
        $this->empresas = $empresas;

        return $this;
    }
}
