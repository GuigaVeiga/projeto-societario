<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use App\Entity\Socio;
use App\Entity\Empresa;

/**
* @Route("/socios", name="socio_")
*/
class SocioController extends AbstractController
{
    /**
     * @Route("/", name="listar", methods={"GET"})
     */
    public function listar(){   
        $socios = $this->getDoctrine()->getRepository(Socio::class)->findAll();
        
        return $this->json([
            'data' => $socios
        ], 200, [], [ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function($object){
            return $object->getNomeSocio();
        }]);
    }

    /**
     * @Route("/{socioId}", name="buscar", methods={"GET"})
     */
    public function buscarPorId($socioId){
        $socio = $this->getDoctrine()->getRepository(Socio::class)->find($socioId);

        return $this->json([
            'data' => $socio
        ], 200, [], [ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function($object){
            return $object->getNomeSocio();
        }]);
    }

    /**
     * @Route("/add", name="adicionar", methods={"POST"})
     */
    public function adicionar(Request $request){
        $data = $request->request->all();

        $socio = new Socio();
        $socio->setNomeSocio($data['nomeSocio']);
        $socio->setEnderecoSocio($data['enderecoSocio']);
        $socio->setTelefoneSocio($data['telefoneSocio']);
        $socio->setTelefoneSocio($data['telefoneSocio']);

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->persist($socio);
        $doctrine->flush();

        return $this->json([
            'data' => 'Socio Criado com Sucesso!'
        ]);
    }

    /**
     * @Route("/{socioId}", name="atualizar", methods={"PUT", "PATCH"})
     */
    public function atualizar($socioId, Request $request){
        $data = $request->request->all();
        $doctrine = $this->getDoctrine();

        $socio = $doctrine->getRepository(Socio::class)->find($socioId);

        if($request->request->has('nomeSocio'))
            $socio->setNomeSocio($data['nomeSocio']);
        
        if($request->request->has('enderecoSocio'))    
            $socio->setEnderecoSocio($data['enderecoSocio']);

        if($request->request->has('telefoneSocio'))
            $socio->setTelefoneSocio($data['telefoneSocio']);

        $manager = $doctrine->getManager();

        
        $manager->flush();

        return $this->json([
            'data' => 'Socio Atualizada com Sucesso!'
        ]);
        
    }

    /**
     * @Route("/{socioId}", name="deletar", methods={"DELETE"})
     */
    public function deletar($socioId){
        $doctrine = $this->getDoctrine();

        $socio = $doctrine->getRepository(Socio::class)->find($socioId);

        $manager = $doctrine->getManager();
        $manager->remove($socio);

        $manager->flush();

        return $this->json([
            'data' => 'Socio removido com Sucesso!'
        ]);
    }

    /**
     * @Route("/addSocio", name="adicionarSocio", methods={"POST"})
     */
    public function adicionarSocio(Request $request){
        $data = $request->request->all();

        $empresa = new Empresa();
        $empresa->setNomeEmpresa($data['nomeEmpresa']);
        $empresa->setEnderecoEmpresa($data['enderecoEmpresa']);
        $empresa->setTelefoneEmpresa($data['telefoneEmpresa']);

        $socio = new Socio();
        $socio->setNomeSocio($data['nomeSocio']);
        $socio->setEnderecoSocio($data['enderecoSocio']);
        $socio->setTelefoneSocio($data['telefoneSocio']);
        $socio->setTelefoneSocio($data['telefoneSocio']);

        $socio->getEmpresas()->add($empresa);

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->persist($empresa);
        $doctrine->persist($socio);
        $doctrine->flush();

        return $this->json([
            'data' => 'Socio Criado com Sucesso!'
        ]);
    }
}
