<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use App\Entity\Empresa;
use App\Entity\Socio;

/**
* @Route("/empresas", name="empresa_")
*/
class EmpresaController extends AbstractController
{
    /**
     * @Route("/", name="listar", methods={"GET"})
     */
    public function listar(){   
        $empresas = $this->getDoctrine()->getRepository(Empresa::class)->findAll();
        
        return $this->json([
            'data' => $empresas
        ], 200, [], [ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function($object){
            return $object->getNomeEmpresa();
        }]);
    }

    /**
     * @Route("/{empresaId}", name="buscar", methods={"GET"})
     */
    public function buscarPorId($empresaId){
        $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($empresaId);

        return $this->json([
            'data' => $empresa
        ], 200, [], [ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function($object){
            return $object->getNomeEmpresa();
        }]);
    }

    /**
     * @Route("/add", name="adicionar", methods={"POST"})
     */
    public function adicionar(Request $request){
        $data = $request->request->all();

        $empresa = new Empresa();
        $empresa->setNomeEmpresa($data['nomeEmpresa']);
        $empresa->setEnderecoEmpresa($data['enderecoEmpresa']);
        $empresa->setTelefoneEmpresa($data['telefoneEmpresa']);

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->persist($empresa);
        $doctrine->flush();

        return $this->json([
            'data' => 'Empresa Criada com Sucesso!'
        ]);
    }

    /**
     * @Route("/{empresaId}", name="atualizar", methods={"PUT", "PATCH"})
     */
    public function atualizar($empresaId, Request $request){
        $data = $request->request->all();
        $doctrine = $this->getDoctrine();

        $empresa = $doctrine->getRepository(Empresa::class)->find($empresaId);

        if($request->request->has('nomeEmpresa'))
            $empresa->setNomeEmpresa($data['nomeEmpresa']);
        
        if($request->request->has('enderecoEmpresa'))    
            $empresa->setEnderecoEmpresa($data['enderecoEmpresa']);

        if($request->request->has('telefoneEmpresa'))
            $empresa->setTelefoneEmpresa($data['telefoneEmpresa']);

        $manager = $doctrine->getManager();

        
        $manager->flush();

        return $this->json([
            'data' => 'Empresa Atualizada com Sucesso!'
        ]);
        
    }

    /**
     * @Route("/{empresaId}", name="deletar", methods={"DELETE"})
     */
    public function deletar($empresaId){
        $doctrine = $this->getDoctrine();

        $empresa = $doctrine->getRepository(Empresa::class)->find($empresaId);

        $manager = $doctrine->getManager();
        $manager->remove($empresa);

        $manager->flush();

        return $this->json([
            'data' => 'Empresa removido com Sucesso!'
        ]);
    }

    /**
     * @Route("/addEmpresa", name="adicionarEmpresa", methods={"POST"})
     */
    public function adicionaEmpresa(Request $request){
        $data = $request->request->all();

        $socio = new Socio();
        $socio->setNomeSocio($data['nomeSocio']);
        $socio->setEnderecoSocio($data['enderecoSocio']);
        $socio->setTelefoneSocio($data['telefoneSocio']);
        $socio->setTelefoneSocio($data['telefoneSocio']);

        $empresa = new Empresa();
        $empresa->setNomeEmpresa($data['nomeEmpresa']);
        $empresa->setEnderecoEmpresa($data['enderecoEmpresa']);
        $empresa->setTelefoneEmpresa($data['telefoneEmpresa']);

        $empresa->getSocios()->add($socio);

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->persist($socio);
        $doctrine->persist($empresa);
        $doctrine->flush();

        return $this->json([
            'data' => 'Socio Criado com Sucesso!'
        ]);
    }



}
