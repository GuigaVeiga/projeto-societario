Projeto de Seleção da Vox


Para rodar o projeto, é necessário: 

    * SGBD Postgree
    * Symfony
    * PHP 7
    * Composer
    * Doctrine ORM
    * Criar Banco com o nome "projeto_vox"
    * Criar Tabelas no Banco com o nome "empresa", "socio" e a tabela intermediaria "socios_empresas"
   

Endpoints Socios:

    * Buscar todos os socios: 
        Method: GET 
        URL: http://127.0.0.1:8000/socios/
    * Buscar socios por id: 
        Method: GET: 
        URL: http://127.0.0.1:8000/socios/{id}
    * Adicionar socios: 
        Method: POST:  
        URL: http://127.0.0.1:8000/socios/add
    * Adicionar socios e suas empresas: 
        Method: POST:  
        URL: http://127.0.0.1:8000/socios/addSocio
    * Atualizar socios: 
        Method: PUT/PACTH: 
        URL: http://127.0.0.1:8000/socios/{id}
    * Deletar socios: 
        Method: DELETE:
        URL: http://127.0.0.1:8000/socios/{id}

Endpoints Empresas:

    * Buscar todas as empresas: 
        Method: GET: 
        URL: http://127.0.0.1:8000/empresas/
    * Buscar empresas por id: 
        Method: GET:
        URL: http://127.0.0.1:8000/empresas/{id}
    * Adicionar empresas: 
        Method: POST:  
        URL: http://127.0.0.1:8000/empresas/add
    * Adicionar empresas e seus socios: 
        Method: POST:
        URL: http://127.0.0.1:8000/empresas/addEmpresa
    * Atualizar empresas: 
        Method: PUT/PACTH: 
        URL: http://127.0.0.1:8000/empresas/{id}
    * Deletar empresas: 
        Method: DELETE: 
        URL: http://127.0.0.1:8000/empresas/{id}
